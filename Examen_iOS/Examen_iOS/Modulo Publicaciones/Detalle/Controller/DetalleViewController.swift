//
//  DetalleViewController.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import UIKit

class DetalleViewController: UIViewController {

    //Views
    @IBOutlet weak var titlePublicacion: UILabel!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var textBody: UITextView!
    
    //Variables
    var publicacion = ModelDBPublicaciones()
    var user = ModelDBUsers()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView();

    }

    //MARK: Personalizar view
    func setupView() {
        //Configuración de aspectos de la vista.
                
        titlePublicacion.text = publicacion.title
        nameUser.text = "Por: " + user.name
        textBody.text = publicacion.body

    }

}
