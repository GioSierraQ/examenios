//
//  ViewController.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 13/07/21.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    //Views
    @IBOutlet weak var fieldSearch: UITextField!
    @IBOutlet weak var tableUsers: UITableView!
    @IBOutlet weak var activityCharge: UIActivityIndicatorView!
    
    //Variables
    var users = [ModelDBUsers]()
    
    //Constant
    let manager = DataManagerExternal()
    let realm = try! Realm()
    let alert = Alerts()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView();
        getDataUsers();

    }

    //MARK: Personalizar view
    func setupView() {
        //Configuración de aspectos de la vista.
        
        textFieldDelegateMain()
        self.tableUsers.register(UINib(nibName: TableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TableViewCell.identifier)
        activityCharge.hidesWhenStopped = true

    }
        
    //MARK: Presentar Users desde local o ingresar lo datos a la BD local desde el API.
    func getDataUsers() {
        activityCharge.startAnimating()
        let searchUsersLocal = realm.objects(ModelDBUsers.self)

        if searchUsersLocal.count == 0 {
            manager.getUsers(onSuccess:{ data, status in
                if status == 200 {
                    let decoder = JSONDecoder()
                    
                    guard let usersData = try? decoder.decode([ModelDBUsers].self, from: data as! Data) else{
                        return
                    }
                    
                    self.users = usersData
                    try! self.realm.write {
                        self.realm.add(self.users)
                    }
                    self.dataTable()
                    
                }else{
                    self.activityCharge.stopAnimating()
                    self.alert.ShowAlertSencilla(title: "Error", message: "Tenemos un problema con la respuesta del servidor, porfavor intente mas tarde.", viewController: self)
                }
            }, onError:{ error in
                self.activityCharge.stopAnimating()
                self.alert.ShowAlertSencilla(title: "Error", message: "No se ha podido conectar al servidor, verifica que tengas internet, si no es asi comunicate con soporte del app.", viewController: self)
            })
        }else{
            self.users = Array(searchUsersLocal)
            dataTable()
        }
        
    }
    
    //MARK: Reload table with data
    func dataTable() {
        activityCharge.stopAnimating()
        tableUsers.reloadData()
    }

}

//MARK: Extension of Class for Configuration table of DataSource and Delegate
extension ViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        cell.setupCell(user: users[indexPath.row], view: self)
        return cell
        
    }
    
}

//MARK: Extension of Class for Configuration of textfield, register, return, onchange and other...
extension ViewController: UITextFieldDelegate {
    
    func textFieldDelegateMain() {
        fieldSearch.delegate = self
        fieldSearch.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        var searchUsersLocal: Results<ModelDBUsers>
        if textField.text == "" {
            searchUsersLocal = realm.objects(ModelDBUsers.self)
        }else{
            searchUsersLocal = realm.objects(ModelDBUsers.self).filter("name contains[c] %@", "\(textField.text!)")
        }

        if searchUsersLocal.count == 0 {
            tableUsers.isHidden = true
        }else{
            tableUsers.isHidden = false
        }
        
        self.users = Array(searchUsersLocal)
        dataTable()
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            self.view.endEditing(true)
            return false
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
