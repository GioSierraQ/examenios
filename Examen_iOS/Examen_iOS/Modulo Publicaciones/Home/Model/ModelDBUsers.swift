//
//  ModelDBUsers.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import Foundation
import RealmSwift

class ModelDBUsers: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phone: String = ""
    
    convenience init(id: Int,name: String,email: String,phone: String) {
        self.init()
        self.id = id
        self.name = name
        self.email = email
        self.phone = phone
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
}
