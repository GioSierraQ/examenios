//
//  ModelDBPublicaciones.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import Foundation
import RealmSwift

class ModelDBPublicaciones: Object, Codable {
    @objc dynamic var id: Int = 0
    @objc dynamic var userId: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var body: String = ""
    
    convenience init(id: Int,userId: Int,title: String,body: String) {
        self.init()
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
}
