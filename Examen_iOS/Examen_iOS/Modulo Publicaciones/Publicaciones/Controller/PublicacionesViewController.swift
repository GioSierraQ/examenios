//
//  PublicacionesViewController.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import UIKit
import RealmSwift

class PublicacionesViewController: UIViewController {

    //Views
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var tablePublicaciones: UITableView!
    @IBOutlet weak var activityCharge: UIActivityIndicatorView!

    //Variables
    var publicaciones = [ModelDBPublicaciones]()
    var user = ModelDBUsers()

    //Constant
    let manager = DataManagerExternal()
    let realm = try! Realm()
    let alert = Alerts()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupView();
        getData();
    }

    //MARK: Personalizar view
    func setupView() {
        //Configuración de aspectos de la vista.
        
        self.tablePublicaciones.register(UINib(nibName: PublicacionesTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PublicacionesTableViewCell.identifier)
        
        tablePublicaciones.largeContentTitle = "Hola"
        nameLabel.text = user.name
        emailLabel.text = user.email
        phoneLabel.text = user.phone
        activityCharge.hidesWhenStopped = true
    }
        
    //MARK: Presentar Users desde local o llenar la BD local con la información del api.
    func getData() {
        activityCharge.startAnimating()
        let searchPublicacionesLocal = realm.objects(ModelDBPublicaciones.self).filter("userId = %@", user.id)

        if searchPublicacionesLocal.count == 0 {
            manager.getPosts(onSuccess:{ data, status in
                if status == 200 {
                    let decoder = JSONDecoder()
                    
                    guard let publicacionesData = try? decoder.decode([ModelDBPublicaciones].self, from: data as! Data) else{
                        return
                    }

                    try! self.realm.write {
                        self.realm.add(publicacionesData)
                    }
                    
                    let searchPublicacionesLocalFilter = self.realm.objects(ModelDBPublicaciones.self).filter("userId = %@", self.user.id)
                    self.publicaciones = Array(searchPublicacionesLocalFilter)
                    self.dataTable()
                    
                }else{
                    self.activityCharge.stopAnimating()
                    self.alert.ShowAlertSencilla(title: "Error", message: "Tenemos un problema con la respuesta del servidor, porfavor intente mas tarde.", viewController: self)
                }
            }, onError:{ error in
                self.activityCharge.stopAnimating()
                self.alert.ShowAlertSencilla(title: "Error", message: "No se ha podido conectar al servidor, verifica que tengas internet, si no es asi comunicate con soporte del app.", viewController: self)
            })
        }else{
            self.publicaciones = Array(searchPublicacionesLocal)
            dataTable()
        }
        
    }
    
    //MARK: Reload table with data
    func dataTable() {
        activityCharge.stopAnimating()
        tablePublicaciones.reloadData()
    }

}

//MARK: Extension of Class for Configuration table of DataSource and Delegate
extension PublicacionesViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return publicaciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PublicacionesTableViewCell.identifier, for: indexPath) as! PublicacionesTableViewCell
        cell.setupCell(publicacion: publicaciones[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = DetalleViewController(nibName: "DetalleViewController", bundle: nil)
        viewController.user = user
        viewController.publicacion = publicaciones[indexPath.row]
        
        self.present(viewController, animated: true, completion: nil)

    }
}
