//
//  TableViewCell.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var buttonPublicaciones: UIButton!
    static let identifier = String(describing: TableViewCell.self)

    weak var view: ViewController?
    var user = ModelDBUsers()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    // MARK: Configura la celda y da formato.
    func setupCell(user: ModelDBUsers, view: ViewController) {
        self.user = user
        self.view = view
        nameLabel.text = user.name
        mailLabel.text = user.email
        phoneLabel.text = user.phone
        buttonPublicaciones.tag = user.id
    }
    
    // MARK: Accion del boton para ver la publicacion enviando la información del usuario.
    @IBAction func buttonPublicaciones(_ sender: Any) {
        
        let viewController = PublicacionesViewController(nibName: "PublicacionesViewController", bundle: nil)
        viewController.user = self.user
        view?.navigationController?.pushViewController(viewController, animated: true)

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
