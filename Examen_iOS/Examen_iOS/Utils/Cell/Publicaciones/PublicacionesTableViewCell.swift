//
//  PublicacionesTableViewCell.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import UIKit

class PublicacionesTableViewCell: UITableViewCell {

    //Views
    @IBOutlet weak var titleLabel: UILabel!
    
    static let identifier = String(describing: PublicacionesTableViewCell.self)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: Configura la celda y da formato.
    func setupCell(publicacion: ModelDBPublicaciones) {
        titleLabel.text = publicacion.title
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
