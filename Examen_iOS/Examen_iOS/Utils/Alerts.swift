//
//  Alerts.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 14/07/21.
//

import Foundation
import UIKit

class Alerts {
    
    // MARK: Muestra dialogo en forma de alerta sencilla.
    func ShowAlertSencilla(title:String,message:String,viewController: UIViewController) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { action in
            print("")
        }))
        viewController.present(alert, animated: true)
        
    }
    
}
