//
//  DataManagerExternal.swift
//  Examen_iOS
//
//  Created by Giovani Sierra quintero on 13/07/21.
//

import Foundation
import Alamofire

class DataManagerExternal {
    
    let dataBase = "https://jsonplaceholder.typicode.com"
    
    // MARK: Conseguir todos los usuarios
    func getUsers(onSuccess: @escaping (Any,Int) -> Void, onError: @escaping(Any) -> Void){
        //Realizar la solicitud por medio de alamofire de la ruta get /users
                
        AF.request(dataBase + "/users", method: .get, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseData {
            response in
            
            switch response.result {
                case .success(let JSON):
                    
                    guard let statusCode = response.response?.statusCode else {
                        return onError("Servidor no responde correctamente.")
                    }
                    
                    if statusCode < 400 {
                        return onSuccess(JSON,statusCode)
                    }else{
                        return onError("Servidor no responde correctamente.")
                    }
            
                case .failure(let error):
                        return onError(error)
                }
            
        }

    }
    
    // MARK: Conseguir todos los posts
    func getPosts(onSuccess: @escaping (Any,Int) -> Void, onError: @escaping(Any) -> Void){
        //Realizar la solicitud por medio de alamofire de la ruta get /posts
                
        AF.request(dataBase + "/posts", method: .get, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseData {
            response in
            
            switch response.result {
                case .success(let Data):
                    
                    guard let statusCode = response.response?.statusCode else {
                        return onError("Servidor no responde correctamente.")
                    }
                    
                    if statusCode < 400 {
                        return onSuccess(Data,statusCode)
                    }else{
                        return onError("Servidor no responde correctamente.")
                    }
            
                case .failure(let error):
                        return onError(error)
                }
            
        }
        
    }
    
    // MARK: Conseguir los posts de usuario especifico del id enviado
    func getPostID(userId:Int, onSuccess: @escaping (Any,Int) -> Void, onError: @escaping(Any) -> Void) {
        //Realizar la solicitud por medio de alamofire de la ruta get /posts?userId= por medio de id

        AF.request(dataBase + "/posts", method: .get, encoding: URLEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseData {
            response in
            
            switch response.result {
                case .success(let Data):
                    
                    guard let statusCode = response.response?.statusCode else {
                        return onError("Servidor no responde correctamente.")
                    }
                    
                    if statusCode < 400 {
                        return onSuccess(Data,statusCode)
                    }else{
                        return onError("Servidor no responde correctamente.")
                    }
            
                case .failure(let error):
                        return onError(error)
                }
            
        }
        
    }
}
